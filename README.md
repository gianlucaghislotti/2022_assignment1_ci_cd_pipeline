# Assignment 1

## Membri
- Ghislotti Gianluca 859242
- Esposito Andrea 859732

## Descrizione Applicativo
Abbiamo creato un'architettura basata su un'API back-end in python e un'applicazione front-end servendoci di node.js.
L'API back-end calcola semplicemente la somma di due numeri restituendo il risultato in formato JSON, il front-end, invece, si occupa di presentare un form per inserire i due numeri scelti. 

## Descrizione Pipeline
Il nostro applicativo come precedentemente descritto, è formato da due parti ben distinte e sviluppate partendo da zero, il tutto sulla sola repository dedicata all'assignment. Detto questo, abbiamo deciso di creare due pipeline "indipendenti", per mantenere un ordine logico e reale del progetto.
In particolare, è stata effettuata una concatenazione delle due pipeline, partendo dall'api di back-end, la quale dovrà essere rilasciata in produzione prima del front-end, in quanto quest'ultimo dovrà effettuare gli integration test.

### Back-end stages
#### build_back-end
Verifica della corretta installazione delle dipendenze.

#### verify_back-end
Verifica correttezza sintattica del codice.

#### unit_test_back-end
Verifica correttezza della funzione interna all'api, usando una funzione in locale con lo stesso corpo.

#### package_back-end
Verifica della corretta creazione del package e del file .whl.

#### release_back-end
Effettua la release del file .whl su PyPI.

#### deploy_back-end
Effettua la release in produzione del codice aggiornato su Heroku.


### Front-end stages

#### build_front-end
Installa le dipendenze necessarie per il progetto.

#### verify_front-end
Analizza sintatticamente il codice con eslint e flow.

#### integration-test
Testa il corretto collegamento con l'api di back-end.

#### package_front-end
Esegue la build, creando la cartella dist contenente i file che andranno ad essere rilasciati.

#### release_front-end
Crea il tag nella repository e incrementa automaticamente la versione del progetto.

#### deploy_front-end
Effettua la release in produzione del codice aggiornato su Heroku.
