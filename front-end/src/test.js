const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest

test("HTTP GET REQUEST TEST", () => {
    var url = "https://sumresolver.herokuapp.com/sum?first=3&second=4"

    var req = new XMLHttpRequest()
    req.open("GET", url, false)
    req.send(null)
    expect(req.status).toBe(200)
    expect(req.readyState).toBe(4)
})