from flask import Flask, request
from flask_cors import CORS, cross_origin
import os

app = Flask(__name__)
CORS(app, support_credentials=True)


@app.route('/sum', methods=['GET'])
@cross_origin(support_credentials=True)
def somma():
    first = int(request.args.get('first'))
    second = int(request.args.get('second'))
    if isinstance(first, int) and isinstance(second, int):
        return {"sum": first + second}

    return {"sum": "Dati non interi."}


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host="0.0.0.0", port=port, debug=True)
