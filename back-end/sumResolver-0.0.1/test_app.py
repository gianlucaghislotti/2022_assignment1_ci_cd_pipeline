def sumResolver(a, b):
    if isinstance(a, int) and isinstance(b, int):
        return a + b

    return "I dati inseriti non sono di tipo intero."


def test_sumResolver():
    assert sumResolver(10, 20) == 30
