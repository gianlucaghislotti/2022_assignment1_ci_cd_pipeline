import setuptools

setuptools.setup(
    include_package_data=True,
    name="sumResolver",
    version="0.0.1",
    description="Package del progetto",
    url="https://gitlab.com/gianlucaghislotti/2022_assignment1_ci_cd_pipeline.git",
    author="G. Ghislotti",
    author_email="g.ghislotti@campus.unimib.it",
    packages=setuptools.find_packages(),
    install_requires=['Flask', 'psycopg2-binary', 'requests', 'pytest', 'prospector', 'Flask-Cors', 'bandit'],
)
